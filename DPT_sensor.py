#DPT_sensor.py
#Version 1.3
#code for the DPT, taking voltage readings and recording averages from 10 datapoints every second, converting to pressure into a data file
#Code heavily modified by JA and CS from the original Ex13_ADC.py

def DPTfunction():

    from ABE_ADCPi import ADCPi           #treat this module as a black box
    import math, smbus, time

    bus=smbus.SMBus(1)    #I2C bus 1 for latest Raspberry-Pis

    address_1_4=0x68      #I2C addresses for each of the 4 channel ADCs on the board (Default 0x68, 0x69)
    address_5_8=0x69      #Find these out using $ sudo i2cdetect -y 1

    adc = ADCPi(bus, address_1_4, address_5_8, 18) #this configures the ADC addresses, bit rate 

    gain=1 # x1 x2 x4 or x8
    adc.set_pga(gain)

#       set bitrate
#	12 = 12 bit (240SPS max)        ** SPS - "Samples per second"
#	14 = 14 bit (60SPS max)
#	16 = 16 bit (15SPS max)
#	18 = 18 bit (3.75SPS max)       Higher bit rates reduce speed 
    rate=16
    adc.setBitRate(rate)

    channel=2 #set to correct channel

#   log=open("DPTlog_raw","a")
#   print "DPT data collection started..."

    start_time=time.time()
    endtime=start_time+1                       #set up timer to execute loop every second
    L = []                                     # set up empty list to populate with data

    while time.time()<endtime:                 #loop to record 10 datapoints a second to the list L
        volt=adc.read_voltage(channel)
        L.append(volt)
        time.sleep(0.1)

    avg_volt=(sum(L))/(len(L))                 #finding the average voltage from L
    avg_Pa=math.trunc(round(avg_volt*50000))   #difference of 1 Volt = 5 Bar from DPT datasheet, then converting to Pascals to 4 sf
#   timenow=time.strftime("%Y-%m-%d %H:%M:%S") #finding the current time
#   print timenow, avg_volt, avg_Pa            #debugging output
#   print>>log, timenow, "Pa:", avg_Pa         #print redirect to log
    avg_flowrate=avg_Pa*10**(-8)+0.0001
    return avg_flowrate
