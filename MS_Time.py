#MS_Time
#This function returns the time and date including milliseconds as a string.
#V1.1 
#Changelog: Tested and made neater vs V1.0
#This code was created by Christie Sims and Johannes Adam, working with and against each other.

#while 1:
def MS_Time():

    import time, math                      #loading required modules

    times=time.time()                      #precise epoch time
    truncatedtime=math.trunc(times)        #epoch time rounded down to seconds
    #print (times)                         #debugging visual output
    #print("\n"+str(truncatedtime))        #debugging visual output

    seconddifference=times-truncatedtime   #sub-second dif
    #print ("\n"+str(seconddifference))    #debugging visual output

    milliseconds=round(seconddifference,3) #sub-second dif rounded to 3dp
    #print("\n"+str(milliseconds))         #debugging visual output

    realmilliseconds=milliseconds*1000     #convert to ms
    truncmilliseconds=math.trunc(realmilliseconds) # number formatting to remove decimal place
    #print("\n"+str(truncmilliseconds))    #debugging visual output

    timestamp=time.strftime("%Y-%m-%d %H:%M:%S")           # create the first part of the final output
    currenttime=str(timestamp+":"+str(truncmilliseconds))  # combine timestamp and ms for final output
    return(currenttime)                    # currenttime is output as a string


# currenttimeoutput=MS_Time()               #debugging visual output
# print(currenttimeoutput)                  #debugging visual output
