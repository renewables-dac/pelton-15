#RPM_sensor.py
#Version 0.0
#measure RPM from ferrous sensor mounted to Pelton wheel
#modified by JA, original code from Ex10_Pulsecounter
import RPi.GPIO as GPIO
import time

#GPIO.setwarnings(False)        #GPIO.setwarnings(False) to suppress any warning messages
GPIO.cleanup()                 #This resets all previous usage of the GPIO pins for our project
GPIO.setmode(GPIO.BCM)         #Set pin assignment based on Broadcom pin numbering (see GPIO map provided)
GPIO.setup(21,GPIO.IN)

oldpinstate=False              #The trick here is to debounce the switch using a sleep delay and a state toggle to register pulses on the rising edge ________|''''''''|_______

output=open("RPMlog","a")      # to create a new file for a log of number of pulses or append existing file

while 1:
    L=[]
    newpinstate = GPIO.input(21)
    start_time=time.time()
    endtime=start_time+1 
    
    while time.time()<endtime:
        if (newpinstate == True) and (oldpinstate == False):    #Pulse triggers even on rising edge (Could be a counter increment - in this case: print detection message)
            L.append("detection")
        
            oldpinstate = True

        if (newpinstate == False) and (oldpinstate == True):    #On Falling edge, the pin state flags are reversed, ready for the next rising edge pulse
            oldpinstate = False    
    
    time.sleep(0.02)

    print(len(L))
    
output.close()
