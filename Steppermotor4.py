#Steppermotor3
#Version 1.1
#the 3rd different code to run stepper motors, works using a simple 4 step sequence
#Modified by JA from the source available from:
# http://www.raspberrypi.org/forums/viewtopic.php?f=49&t=55580

#import modules
def Stepbackwards():

    import RPi.GPIO as GPIO
    import time
    
    # Variables
    delay = 0.005
    steps = 100
    
    #set GPIO pinmode and suppress warnings
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)

    # Enable pins for IN1-4 to control step sequence
    coil_A_1_pin = 23
    coil_A_2_pin = 24
    coil_B_1_pin = 18
    coil_B_2_pin = 25

    # Set pin states
    GPIO.setup(coil_A_1_pin, GPIO.OUT)
    GPIO.setup(coil_A_2_pin, GPIO.OUT)
    GPIO.setup(coil_B_1_pin, GPIO.OUT)
    GPIO.setup(coil_B_2_pin, GPIO.OUT)

    # Function for step sequence
    def setStep(w1, w2, w3, w4):
     GPIO.output(coil_A_1_pin, w1)
     GPIO.output(coil_A_2_pin, w2)
     GPIO.output(coil_B_1_pin, w3)
     GPIO.output(coil_B_2_pin, w4)

    # loop through forward step sequence based on number of steps
    for i in range(0, steps):
        setStep(1,0,1,0)
        time.sleep(delay)
        setStep(0,1,1,0)
        time.sleep(delay)
        setStep(0,1,0,1)
        time.sleep(delay)
        setStep(1,0,0,1)
        time.sleep(delay)


    # loop through reverse step sequence
    #for i in range(0, steps):
     #   setStep(1,0,0,1)
      #  time.sleep(delay)
     #   setStep(0,1,0,1)
     #   time.sleep(delay)
     #   setStep(0,1,1,0)
     #   time.sleep(delay)
      #  setStep(1,0,1,0)
       # time.sleep(delay)

    #set all pins LOW to stop current flow in motor when idle
    setStep(0,0,0,0)
    return
