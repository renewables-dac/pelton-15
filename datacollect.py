#datacollect.py
#Version 1.0
#data collection function
#written by JA

while 1:

    import time
    import pressure_sensor as Pressure
    import workingrpmscript as RPM
    import DPT_sensor as DPT
    import math
    import Steppermotor3 as Stepforwards
    import Steppermotor4 as Stepbackwards
    
    def datacollect():
        currentRPM=RPM.RPMfunction()
        currentpressure=Pressure.pressurefunction()
        currentDPT=DPT.DPTfunction()
        timestamp=time.strftime("%Y-%m-%d %H:%M:%S")
#    print timestamp,currentRPM,currentpressure,currentDPT

#return is Time, RPM, Pressure, Flowrate
        return timestamp,currentRPM,currentpressure,currentDPT

    timestamp,currentRPM,currentpressure,currentDPT=datacollect()
    
    filename="Results"
    
    output=open(filename,"a")
    xml_output="<Data_Point><Time_stamp>"+timestamp+"</Time_stamp><RPM>"+str(currentRPM)+"/RPM><Pressure(kPa)>"+str(currentpressure)+"</Pressure(kPa)><Flowrate>"+str(currentDPT)+"</Flowrate></Data_point>"
    output.write(xml_output)
    output.close
    

    L=datacollect()
    #print L[1], L[2], L[3]
    print xml_output
 
    pressure_1=L[2]*10000
    pressure_2=100000
    density=1000
    flow=L[3]
    velocity_1=flow/0.027
    top1=2*pressure_1
   
    top2=velocity_1**2
    top2b=density*top2
   
    top3=2*pressure_2

    top_all=top1+top2b-top3
 
    fraction=top_all/density
    try:
        velocity_2=math.sqrt(fraction)
    except:
        print("machine not running")
    RPM=L[1]

    linear_velocity=0.06*RPM*0.10472
    
    try:
        print ("linear velocity"+str(linear_velocity)+"    velocity of jet"+str(velocity_2))
    except:
        print ("Ensure machine is on")
#    if linear_velocity>velocity_2:
       #run motor
#       Stepforwards()
#    if linear_velocity<velocity_2:
       #run motor
#       Stepbackwards
    #print (L[1]+"RPM   ", L[2]+"Pressure (kPa)   ", L[3]+"Flow rate(m3/s)")
    print L[1], L[2], L[3]
    print ("RPM kPa    m3/s")
    time.sleep(3)


