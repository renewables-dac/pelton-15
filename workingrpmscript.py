 #### Example 10.2 - Pulse counting ############

#So far we have concentrated on very general operations in Python concerning variable and file handling, and also scripting logic and flow.

#Now for the more juicy bits - using connected sensor hardware to capture your environmental / process data


#Ex10.2 - Reed switch pulse counter

# Before running this script, ensure you have connected your reed switch as in breadboard circuit 10.1 with the reed connected across 3.3V and GPIO 17
# Out of interest, you could try running it with nothing connected. GPIO 17 would be "floating", and lots of false pulses can be expected.
# For this reason, the GPIO pins should always be used with PULLUP (to +3.3V) or PULL DOWN (to 0V GND) resistors. A 10K ohm resistor is usually a good choice for signal input pullup/down.

def RPMfunction():

    import RPi.GPIO as GPIO
    import time

##    print "calling RPMfunction"

    GPIO.setwarnings(False)            #GPIO.setwarnings(False) to suppress any warning messages
    GPIO.setmode(GPIO.BCM)            #Set pin assignment based on Broadcom pin numbering (see GPIO map provided)
    GPIO.setup(21,GPIO.IN)

    oldpinstate=True                 #The trick here is to debounce the switch using a sleep delay and a state toggle to register pulses on the rising edge ________|''''''''|_______
    start_time=time.time()
    endtime=start_time+5            #Setup pulse detector to run for 5 seconds  just for this example.
    pulsecount = 0

    while time.time()<endtime:        #Loop pulse detector for 5 seconds
        newpinstate = GPIO.input(21)
    
        if (newpinstate == True) and (oldpinstate == False):    #Pulse triggers even on rising edge (Could be a counter increment - in this case: print detection message)
            pulsecount=pulsecount+1
            oldpinstate = True
    
        if (newpinstate == False) and (oldpinstate == True):    #On Falling edge, the pin state flags are reversed, ready for the next rising edge pulse
            oldpinstate = False

    actual_RPM=pulsecount*12
##    print "RPMfunction called successfully"
    return actual_RPM


